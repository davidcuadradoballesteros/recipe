DROP TABLE IF EXISTS recipe;
DROP TABLE IF EXISTS ingredient;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS recipeToIngredient;
DROP TABLE IF EXISTS recipeToImage;
DROP TABLE IF EXISTS ingredientToImage;

CREATE TABLE image(
    image_surrogate_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    image_id VARCHAR(36) NOT NULL,
    image_name VARCHAR(100) NOT NULL,
    image_url VARCHAR(255) NOT NULL
);

CREATE TABLE recipe(
    recipe_surrogate_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    recipe_id VARCHAR(36) NOT NULL,
    recipe_name VARCHAR(100) NOT NULL,
    recipe_image INT,
    recipe_description TEXT,
    FOREIGN KEY fk_recipe_image(recipe_image)
        REFERENCES image(image_surrogate_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE ingredientFamily(
    ingredientFamily_surrogate_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ingredientFamily_id VARCHAR(36) NOT NULL,
    ingredientFamily_name VARCHAR(100) NOT NULL,
    ingredientFamily_description TEXT,
    ingredientFamily_image INT,
    FOREIGN KEY fk_ingredientFamily_image(ingredientFamily_image)
        REFERENCES image(image_surrogate_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE ingredient(
    ingredient_surrogate_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ingredient_id VARCHAR(36) NOT NULL,
    ingredient_name VARCHAR(100) NOT NULL,
    ingredient_description TEXT,
    ingredient_family INT,
    ingredient_image INT,
    FOREIGN KEY fk_ingredientFamily(ingredient_family)
        REFERENCES ingredientFamily(ingredientFamily_surrogate_id),
    FOREIGN KEY fk_ingredient_image(ingredient_image)
        REFERENCES image(image_surrogate_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE recipeToIngredient(
    recipeToIngredient_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    recipeToIngredient_recipe_id INT NOT NULL,
    recipeToIngredient_ingredient_id INT NOT NULL,
    FOREIGN KEY fk_recipeToIngredient_ingredient(recipeToIngredient_ingredient_id)
        REFERENCES ingredient(ingredient_surrogate_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY fk_recipeToIngredient_recipe(recipeToIngredient_recipe_id)
        REFERENCES recipe(recipe_surrogate_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);